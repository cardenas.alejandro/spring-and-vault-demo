### Setup Vault and Database

The code contains a `docker-compose` 
that will start up vault and postgres, the file is :
`vault.docker-compose.yml` and could execute with the following command:

``` shell script
docker-compose -f vault.docker-compose.yml up
```

The default Vault token is : `root`
The default Postgres username/password is: `jconf2020/jconf2020`

Once the container is running we could identify it with:

```
docker ps
```

Once we identify the Vault container, we could connect to the container and execute the commands contained in the script: `vault.sh`

Connect to the container:
```shell script
docker exec -it <containerId> sh
```

After execute the last command:

``` shell script
vault token create -policy=dev
```

You could use the token created:

```shell script
/ # vault token create -policy=dev
Key                  Value
---                  -----
token                <token>
token_accessor       <token_accessor>
token_duration       768h
token_renewable      true
token_policies       ["default" "dev"]
identity_policies    []
policies             ["default" "dev"]
```

Then set up the token in the `bootstrap.yml` :

```yaml
spring:
  application:
    name: springandvault
  cloud:
    vault:
      authentication: TOKEN
      token: <token>
```