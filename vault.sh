#enabled log audit
vault audit enable file file_path=/vault/logs/vault_audit.log

echo 'path "secret/data/springandvault/dev" {
  capabilities = ["read"]
}
path "secret/data/springandvault" {
  capabilities = ["read"]
}
path "secret/data/application/dev" {
  capabilities = ["read"]
}
path "secret/data/application" {
  capabilities = ["read"]
}
path "transit/decrypt/demo" {
  capabilities = ["update"]
}
path "transit/encrypt/demo" {
  capabilities = ["update"]
}
path "database/creds/demo" {
  capabilities = ["read"]
}
path "sys/renew/*" {
  capabilities = ["update"]
}' | vault policy write dev -

#enable transit demo
vault secrets enable transit

#create transit to demo
vault write -f transit/keys/demo

#Mount DB backend
vault secrets enable database

#enabled db access from vault
vault write database/config/postgresql \
  plugin_name=postgresql-database-plugin \
  allowed_roles="*" \
  connection_url="postgresql://{{username}}:{{password}}@db:5432/jconf2020?sslmode=disable" \
  username="jconf2020" \
  password="jconf2020"

#Create the DB order role
vault write database/roles/demo \
  db_name=postgresql \
  creation_statements="CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}'; GRANT USAGE ON ALL SEQUENCES IN SCHEMA public TO \"{{name}}\"; GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO \"{{name}}\";" \
  default_ttl="1h" \
  max_ttl="24h"

vault token create -policy=dev