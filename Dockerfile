FROM openjdk:11-slim
ADD target/*.jar demo.jar
CMD [ "java", "-jar", "/demo.jar" ]