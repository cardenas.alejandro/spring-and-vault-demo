package com.jconf.v2020.springandvault.demo.service;

import com.jconf.v2020.springandvault.demo.domain.Invoice;
import com.jconf.v2020.springandvault.demo.exception.NotFoundException;
import com.jconf.v2020.springandvault.demo.repository.InvoiceRepository;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class InvoiceService {

  private InvoiceRepository invoiceRepository;

  public InvoiceService(
    InvoiceRepository invoiceRepository) {
    this.invoiceRepository = invoiceRepository;
  }

  public List<Invoice> getAll() {
    List<Invoice> invoices = new LinkedList<>();
    invoiceRepository.findAll().forEach(invoices::add);
    return invoices;
  }

  public Invoice create(Invoice invoice) {
    return invoiceRepository.save(invoice);
  }

  public Invoice getById(Long id) {
    return invoiceRepository.findById(id).orElseThrow(NotFoundException::new);
  }

}
