package com.jconf.v2020.springandvault.demo.repository;

import com.jconf.v2020.springandvault.demo.domain.Invoice;
import org.springframework.data.repository.CrudRepository;

public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

}
