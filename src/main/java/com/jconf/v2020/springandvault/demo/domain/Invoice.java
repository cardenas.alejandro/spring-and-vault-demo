package com.jconf.v2020.springandvault.demo.domain;

import com.jconf.v2020.springandvault.demo.converter.TransitConverter;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Data
@NoArgsConstructor
@Entity
@Table(name = "invoice")
public class Invoice {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;
  @Convert(converter = TransitConverter.class)
  private String treasureSeal;
  private String name;
  @NonNull
  private Double subtotal;
  @Transient
  private Double total;

}
