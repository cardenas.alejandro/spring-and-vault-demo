package com.jconf.v2020.springandvault.demo.controller;

import com.jconf.v2020.springandvault.demo.domain.Invoice;
import com.jconf.v2020.springandvault.demo.service.InvoiceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/")
public class InvoiceController {

  private InvoiceService invoiceService;

  @GetMapping
  public List<Invoice> getInvoices() {
    return invoiceService.getAll();
  }

  @GetMapping("/{id}")
  public ResponseEntity<Invoice> getInvoiceById(@PathVariable Long id) {

    Invoice invoice = invoiceService.getById(id);

    return ResponseEntity.ok(invoice);
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity createInvoice(@RequestBody Invoice invoice) {
    Invoice savedInvoice = invoiceService.create(invoice);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
      .path("/{id}")
      .buildAndExpand(savedInvoice.getId())
      .toUri();

    return ResponseEntity.created(location).build();
  }
}
