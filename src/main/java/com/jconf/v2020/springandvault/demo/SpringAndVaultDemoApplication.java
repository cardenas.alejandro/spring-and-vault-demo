package com.jconf.v2020.springandvault.demo;

import com.jconf.v2020.springandvault.demo.config.VaultConfigProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(VaultConfigProperties.class)
public class SpringAndVaultDemoApplication implements CommandLineRunner {

  @Value("${spring.datasource.username}")
  private String dbUser;

  @Value("${spring.datasource.password}")
  private String dbPass;

  private final VaultConfigProperties configuration;

  public SpringAndVaultDemoApplication(VaultConfigProperties configuration) {
    this.configuration = configuration;
  }

  public static void main(String[] args) {
    SpringApplication.run(SpringAndVaultDemoApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {

    Logger logger = LoggerFactory.getLogger(SpringAndVaultDemoApplication.class);

    logger.info("----------------------------------------");
    logger.info("Configuration properties");
    logger.info("   demo.username is {}", configuration.getUsername());
    logger.info("   demo.password is {}", configuration.getPassword());

    logger.info("   demo.password is {}", dbPass);
    logger.info("   demo.username is {}", dbUser);
    logger.info("----------------------------------------");
  }

}
