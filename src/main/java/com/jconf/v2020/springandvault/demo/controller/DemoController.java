package com.jconf.v2020.springandvault.demo.controller;

import com.jconf.v2020.springandvault.demo.config.VaultConfigProperties;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@RestController
@RequestMapping("/demo")
public class DemoController {

  private VaultConfigProperties properties;

  @GetMapping
  public Map<String, String> getDemoProperties() {
    Map<String, String> demoProperties = new HashMap<>();
    demoProperties.put("Username", properties.getUsername());
    demoProperties.put("Password", properties.getPassword());
    return demoProperties;
  }

}
